.def inout = r16
.def counter = r17
.def workhorse = r18

.org    0x0000
		rjmp    setup
.org	0x0020
		rjmp ISR_OV0
.org    0x0100
		


setup:  ldi		inout, 0xFF
		out		DDRD, inout
		ldi		workhorse, 0b00000000
		out		TCCR0A, workhorse
		ldi		workhorse, 0b00000001
		out		TCCR0B, workhorse
		ldi     workhorse, 0b00000001
		sts		TIMSK0, workhorse
		sei
		rjmp	loop


loop:   out		PORTD,	counter
		rjmp    loop


ISR_OV0:
			push	workhorse
			lds		workhorse, SREG
			push	workhorse
			inc		counter
			pop		workhorse
			sts		SREG, workhorse
			pop		workhorse
			reti