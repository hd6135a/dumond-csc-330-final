;Heather Dumond
;13 December 2016
;CSC-330 Final Project

;Monophonic Synth Code: 8-bit ADC to set freq, 8-bit DAC for output

; Stores to program memory
.cseg 

; Defines program constants
.def	in_out    = R16		; establishes register 6 as I/O
.def	counter   = R17		; counter stored to reg 17
.def	workhorse = R18		; sets register 18
.def	amp_value = R19		; amplitude value stored in reg 18
.def    adm_value = R20		; stores ADCH value in  a register to be called later	
.def	reg_count = R21		; keeps track of register count


.org    0x0000				; sets origin	 
		rjmp		setup	; jmps to setup
.org	0x0020		 		; set address 00 in program memory
		rjmp		ISR_OV0 ; jumps to interrupt ISR_OV0

.org	0x002A				; sets address 00 in program memory
		rjmp		ISR_ADM	; jmps to ADMUX 	

.org   	0x0100				; sets address 01 in program memory

; Sets macro pointer

.macro pointer 				
		ldi @0,		low(@2)
		ldi @1,		high(@2)
.endmacro

;basic setup -- sets up initial registers and timer count
		
setup: 	
		ldi			in_out, 0xFF					; establishes in out
		out			DDRD, in_out					; sets output port 
		ldi			in_out, 0b11111110
		ldi			workhorse, 0b00000000			; sets value to workhorse
		out			TCCR0A, workhorse				; sets timer 
		ldi			workhorse, 0b00000011		
		out			TCCR0B, workhorse				; ends timer
		sts			TIMSK0, workhorse				; sets timer interrupt rate
		ldi			workhorse, 0b01110101 
		sts			ADMUX, workhorse				; ADC conversion
		ldi			workhorse, 0b11101111
		sts			ADLAR, workhorse				; enables ADC
 		sei		
		rjmp		sawtooth_wave 					; functional sawtooth wave
	;	rjmp		sin_point						; uncomment for sin wave
	;	rjmp		tri_point						; uncomment for triangle wave

;begins sawtooth loop 
sawtooth_wave: 
		out	  		PORTD,	counter 				; declares output to PORTD for each timer cycle		
		rjmp    	sawtooth_wave					; jumps to beginning of sawtooth loop
		
;pointer for triangle loop
tri_Pointer: pointer ZL, ZH, tri*2
;loop for triangle
tri_Loop:					 					; begins loop
		cpi			reg_count, 255				; keeps track which register you're in, in lookup table
		breq		tri_Pointer
		rjmp		tri_Loop 					; starts triangle loop again so keeps going
		
;pointer for sin 		
sin_Pointer: pointer ZL, ZH, sin*2
; Sin Loop
sin_Loop:										; begins loop
		cpi			reg-count, 255				; keeps track which register you're in, in lookup table
		breq		sin_Pointer					; break if not equal , loops back to beginning of db table
		rjmp		sin_Loop 					; starts triangle loop again so keeps going

; Sets up first interrupt service routines 
ISR_OV0:			
		out 		PORTD, amp_value			; declares output to PORTD for each timer cycle	
		lpm			amp_value, Z+				; Increments Z
		inc			counter						
		out			TCNT0, adm_value			; put potentiometer value into TCTN0 for variable waves
		reti 

; ADMUX interrupt 
ISR_ADM:
		lds 		adm_value, ADCH				; loads ADMUX value into the preload reg each time ADC converter completes cycle
		reti									; resets

; Lookup tables for triangle, sawtooth, and sin waves

tri:	.db			0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40,42,44,46,48,50,52,54,56,58,60,62,64,66,68,70,72,74,76,78,80,82,84,86,88,90,92,94,96,98,100,102,104,106,108,110,112,114,116,118,120,122,124,126,128,129,131,133,135,137,139,141,143,145,147,149,151,153,155,157,159,161,163,165,167,169,171,173,175,177,179,181,183,185,187,189,191,193,195,197,199,201,203,205,207,209,211,213,215,217,219,221,223,225,227,229,231,233,235,237,239,241,243,245,247,249,251,253,255,253,251,249,247,245,243,241,239,237,235,233,231,229,227,225,223,221,219,217,215,213,211,209,207,205,203,201,199,197,195,193,191,189,187,185,183,181,179,177,175,173,171,169,167,165,163,161,159,157,155,153,151,149,147,145,143,141,139,137,135,133,131,129,128,126,124,122,120,118,116,114,112,110,108,106,104,102,100,98,96,94,92,90,88,86,84,82,80,78,76,74,72,70,68,66,64,62,60,58,56,54,52,50,48,46,44,42,40,38,36,34,32,30,28,26,24,22,20,18,16,14,12,10,8,6,4,2

sin:	.db			128,131,134,137,140,143,146,149,152,155,158,162,165,167,170,173,176,179,182,185,188,190,193,196,198,201,203,206,208,211,213,215,218,220,222,224,226,228,230,232,234,235,237,238,240,241,243,244,245,246,248,249,250,250,251,252,253,253,254,254,254,255,255,255,255,255,255,255,254,254,254,253,253,252,251,250,250,249,248,246,245,244,243,241,240,238,237,235,234,232,230,228,226,224,222,220,218,215,213,211,208,206,203,201,198,196,193,190,188,185,182,179,176,173,170,167,165,162,158,155,152,149,146,143,140,137,134,131,128,124,121,118,115,112,109,106,103,100,97,93,90,88,85,82,79,76,73,70,67,65,62,59,57,54,52,49,47,44,42,40,37,35,33,31,29,27,25,23,21,20,18,17,15,14,12,11,10,9,7,6,5,5,4,3,2,2,1,1,1,0,0,0,0,0,0,0,1,1,1,2,2,3,4,5,5,6,7,9,10,11,12,14,15,17,18,20,21,23,25,27,29,31,33,35,37,40,42,44,47,49,52,54,57,59,62,65,67,70,73,76,79,82,85,88,90,93,97,100,103,106,109,112,115,118,121,124